import { UserModel } from 'app/models';

export namespace AuthActions {
  export enum Type {
    LOGIN_REQUEST = 'LOGIN_REQUEST',
    LOGIN = 'LOGIN',
    LOGIN_SUCCESS = 'LOGIN_SUCCESS',
    LOGIN_FAILED = 'LOGIN_FAILED',
    LOGOUT = 'LOGOUT',
    DELETE_TOKEN = 'DELETE_TOKEN'
  }
  export const requestLogin = (payload: { email: string; password: string }) => ({ type: Type.LOGIN_REQUEST, payload });
  export const login = () => ({ type: Type.LOGIN });
  export const loginSuccess = (payload: Partial<UserModel>) => ({ type: Type.LOGIN_SUCCESS, payload });
  export const loginFailed = (payload: Partial<UserModel>) => ({ type: Type.LOGIN_FAILED, payload });
  export const logout = () => ({ type: Type.LOGOUT });
  export const deleteToken = () => ({ type: Type.DELETE_TOKEN });
}
