import { all } from 'redux-saga/effects';
import { loginFlow } from 'app/saga/authenticationSaga';

// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all([loginFlow()]);
}
