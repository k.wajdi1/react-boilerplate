import { call, cancel, cancelled, fork, put, take } from 'redux-saga/effects';
import { UserModel } from 'app/models';
import { AuthActions } from 'app/actions';

function fakeAPICall(email: string, password: string) {
  return new Promise((resolve) => {
    const user: Partial<UserModel> = {
      userName: 'test',
      firstName: 'test',
      token: 'this is a token'
    };
    setTimeout(() => {
      resolve(user);
    }, 1000);
  });
}

const loginRequest = function* (email: string, password: string) {
  try {
    yield put(AuthActions.login());
    const user: Partial<UserModel> = yield call(fakeAPICall, email, password);
    yield put(AuthActions.loginSuccess(user));
  } catch (error) {
    yield put(AuthActions.loginFailed({ error: error.message }));
  } finally {
    if (yield cancelled()) {
      yield put({ type: 'LOGIN_CANCELLED' });
    }
  }
};

export const loginFlow = function* () {
  while (true) {
    const { user, password } = yield take([AuthActions.Type.LOGIN_REQUEST]);
    const task = yield fork(loginRequest, user, password);
    const action = yield take([AuthActions.Type.LOGOUT]);
    if (action.type === AuthActions.Type.LOGOUT) {
      yield cancel(task);
      yield put({ type: AuthActions.Type.DELETE_TOKEN });
    }
  }
};
