import React, { SyntheticEvent, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { Button } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { AuthActions } from 'app/actions';
import { RootState } from 'app/reducers';
import ROUTES from 'app/routes';
import style from './style.scss';

namespace LoginOwenProps {
  export interface Props extends RouteComponentProps<void> {}
  export interface LoginPayload {
    email: string;
    password: string;
  }
}

export const Login = ({ history, location }: LoginOwenProps.Props) => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);
  useEffect(() => {
    if (user.token) {
      history.replace(ROUTES.HOME);
    }
  }, [user.token]);
  const handleValidSubmit = (event: SyntheticEvent, { email, password }: LoginOwenProps.LoginPayload) => {
    dispatch(AuthActions.requestLogin({ email, password }));
  };

  return (
    <div className={style.loginContainer}>
      <AvForm onValidSubmit={handleValidSubmit}>
        <AvField name="email" label="Email Address" type="email" required />
        <AvField name="password" autoComplete="true" label="Password" type="password" required />
        <Button color="primary">Login</Button>
      </AvForm>
    </div>
  );
};
