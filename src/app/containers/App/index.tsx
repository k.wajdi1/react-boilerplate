import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import { Header } from 'app/components';
import { RootState } from 'app/reducers';
import { useDispatch, useSelector } from 'react-redux';
import ROUTES from 'app/routes';
import style from './style.scss';
import { AuthActions } from 'app/actions';

namespace AppOwenProps {
  export interface Props extends RouteComponentProps<void> {}
}

export const App = ({ history, location }: AppOwenProps.Props) => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.user);

  useEffect(() => {
    if (!user.token) {
      history.replace(ROUTES.LOGIN);
    }
  }, [user.token]);

  const handleDisconnect = ()=> {
    dispatch(AuthActions.logout());
  };

  return (
    <>
      <Header username="boilerplate" onDisconnect={handleDisconnect}/>
      <Container fluid={true}>
        <Row className={style.row}>
          <Col>
            <div className={style.content}>
              Hello world
            </div>
          </Col>
        </Row>
      </Container>
    </>
  );
};
