import { handleActions } from 'redux-actions';
import { AuthActions } from 'app/actions/authentication';
import { UserModel } from 'app/models';
import { RootState } from './state';

const initialState: RootState.UserState = {
  userName: '',
  firstName: '',
  lastName: '',
  isLoading: false,
  error: '',
  token: ''
};

export const userReducer = handleActions<RootState.UserState, UserModel>(
  {
    [AuthActions.Type.LOGIN]: (state) => {
      return {
        ...state,
        isLoading: true
      };
    },
    [AuthActions.Type.LOGIN_SUCCESS]: (state, action) => {
      return {
        ...state,
        ...action.payload,
        isLoading: false
      };
    },
    [AuthActions.Type.LOGIN_FAILED]: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: action.payload.error
      };
    },
    [AuthActions.Type.DELETE_TOKEN]: () => {
      return {
        ...initialState
      };
    }
  },
  initialState
);
