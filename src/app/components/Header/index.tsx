import React from 'react';
import { Navbar, NavbarBrand, NavbarText } from 'reactstrap';
import style from './style.scss';
namespace HeaderOwenProps {
  export interface Props {
    username: string;
    onDisconnect: ()=> void
  }
}

export const Header = ({ username, onDisconnect }: HeaderOwenProps.Props) => {
  return (
    <Navbar color="light" light expand="md d-flex justify-content-between">
      <NavbarBrand href="/">HELLO {username}</NavbarBrand>
      <NavbarText className={style.logout} onClick={onDisconnect}>Logout</NavbarText>
    </Navbar>
  );
};
