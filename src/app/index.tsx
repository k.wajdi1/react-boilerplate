import React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { App as TodoApp } from 'app/containers/App';
import { Login } from 'app/containers/Login';
import { hot } from 'react-hot-loader';
import ROUTES from 'app/routes';

export const App = hot(module)(() => (
  <HashRouter>
    <Switch>
      <Route path={ROUTES.HOME} exact={true} component={TodoApp} />
      <Route path={ROUTES.LOGIN} exact={true} component={Login} />
    </Switch>
  </HashRouter>
));
