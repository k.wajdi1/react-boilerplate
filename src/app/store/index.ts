import { Store, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { RootState, rootReducer } from 'app/reducers';
import 'regenerator-runtime/runtime';
import { logger } from 'app/middleware';
import rootSaga from 'app/saga';

const sagaMiddleware = createSagaMiddleware();

export function configureStore(initialState?: RootState): Store<RootState> {
  let middleware = applyMiddleware(sagaMiddleware, logger);

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(rootReducer as any, initialState as any, middleware) as Store<RootState>;

  sagaMiddleware.run(rootSaga);

  if (module.hot) {
    module.hot.accept('app/reducers', () => {
      const nextReducer = require('app/reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
