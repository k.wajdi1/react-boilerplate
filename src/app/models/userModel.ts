export interface UserModel {
  userName: string;
  firstName: string;
  lastName: string;
  isLoading: boolean;
  error: string;
  token: string;
}
